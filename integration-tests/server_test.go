package tests

import (
	"context"
	"testing"

	pb "gitlab.com/slomek/golab2018/proto"
)

func TestServer(t *testing.T) {
	cli, err := NewPowerClient("localhost:8000")
	if err != nil {
		t.Fatal(err)
	}

	resp, err := cli.CalculatePower(context.Background(), &pb.CalculatePowerRequest{
		Base:  3.0,
		Power: 2.0,
	})
	if err != nil {
		t.Fatalf("Error: %v", err)
	}
	if want, got := 9.0, resp.GetResult(); want != got {
		t.Errorf("Bad result, expeted %v, got: %v", want, got)
	}
}

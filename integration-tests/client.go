package tests

import (
	"fmt"
	"log"

	pb "gitlab.com/slomek/golab2018/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

var _ pb.PowerClient = &powerClient{}

type powerClient struct {
	pb.PowerClient
}

func NewPowerClient(serverAddr string) (*powerClient, error) {
	creds, err := credentials.NewClientTLSFromFile("../server/server-cert.pem", "")
	if err != nil {
		return nil, fmt.Errorf("cert load error: %s", err)
	}

	conn, err := grpc.Dial(serverAddr, grpc.WithTransportCredentials(creds))
	if err != nil {
		log.Fatalf("Failed to start gRPC connection: %v", err)
	}

	return &powerClient{
		PowerClient: pb.NewPowerClient(conn),
	}, nil
}

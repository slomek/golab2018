package proto

import fmt "fmt"

func (req *CalculatePowerRequest) Validate() error {
	if req.GetBase() > 100 {
		return fmt.Errorf("invalid request: expected base smaller than 100, got: %f", req.GetBase())
	}
	if req.GetPower() > 10 {
		return fmt.Errorf("invalid request: expected base smaller than 10, got: %f", req.GetPower())
	}
	return nil
}

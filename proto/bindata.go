// Code generated by go-bindata. DO NOT EDIT.
// sources:
// service.swagger.json

package proto

import (
	"bytes"
	"compress/gzip"
	"crypto/sha256"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"
)

func bindataRead(data []byte, name string) ([]byte, error) {
	gz, err := gzip.NewReader(bytes.NewBuffer(data))
	if err != nil {
		return nil, fmt.Errorf("Read %q: %v", name, err)
	}

	var buf bytes.Buffer
	_, err = io.Copy(&buf, gz)
	clErr := gz.Close()

	if err != nil {
		return nil, fmt.Errorf("Read %q: %v", name, err)
	}
	if clErr != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

type asset struct {
	bytes  []byte
	info   os.FileInfo
	digest [sha256.Size]byte
}

type bindataFileInfo struct {
	name    string
	size    int64
	mode    os.FileMode
	modTime time.Time
}

func (fi bindataFileInfo) Name() string {
	return fi.name
}
func (fi bindataFileInfo) Size() int64 {
	return fi.size
}
func (fi bindataFileInfo) Mode() os.FileMode {
	return fi.mode
}
func (fi bindataFileInfo) ModTime() time.Time {
	return fi.modTime
}
func (fi bindataFileInfo) IsDir() bool {
	return false
}
func (fi bindataFileInfo) Sys() interface{} {
	return nil
}

var _serviceSwaggerJson = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\xc4\x53\xb1\x6e\xdc\x30\x0c\xdd\xfd\x15\x04\xdb\xf1\x10\x1f\x32\xde\xda\xa9\x5b\xd1\xb5\xc8\xa0\x93\x69\x9f\x02\x5b\x52\x48\x2a\x41\x50\xf8\xdf\x0b\xc9\x97\xb3\xad\xb4\x69\xb7\x7a\xa2\xcc\xc7\x07\xbe\xa7\xa7\x9f\x0d\x00\xca\x8b\x19\x06\x62\x3c\x01\xde\xdf\x1d\xf1\x90\xff\x39\xdf\x07\x3c\x41\xee\x03\xa0\x3a\x1d\x29\xf7\x23\x07\x0d\xad\x10\x3f\x3b\x4b\x77\xe5\x54\xf0\x00\xf8\x4c\x2c\x2e\xf8\x8c\xba\x96\xe0\x83\x82\x90\x62\x03\x30\x17\x56\xb1\x17\x9a\x48\xf0\x04\x3f\x96\xa1\x8b\x6a\x7c\x23\xc8\xb5\x64\xec\x43\xc1\xda\xe0\x25\xed\xc0\x26\xc6\xd1\x59\xa3\x2e\xf8\xf6\x51\x82\x5f\xb1\x91\x43\x97\xec\x3f\x62\x8d\x5e\x64\x95\xd6\xc6\xf0\x52\xb4\x2f\x67\x00\x1c\x48\x37\x47\x00\x0c\x91\xb8\x30\x7d\xed\xb2\xba\x2f\x66\xb4\x69\x34\x4a\xdf\xca\xe4\x61\x05\x32\x49\x0c\x5e\x48\x76\xf3\x00\x78\x7f\x3c\x56\xbf\x00\xb0\x23\xb1\xec\xa2\x5e\x4d\xdb\x10\x95\x76\xf1\xca\xbc\x1b\x03\xc0\xcf\x4c\x7d\x9e\xf8\xd4\x76\xd4\x3b\xef\x32\x83\xb4\xfb\xb5\xbe\x5f\x57\xc1\xdd\xf0\xdc\xfc\xae\x9e\x37\x12\xa2\x61\x33\x91\x12\xaf\x5e\x2e\x5f\xb5\xbc\x37\x53\x09\xc4\xd9\x08\xd5\x9b\xbb\xa2\xe7\x29\x11\xbf\xd6\x2d\xa6\xa7\xe4\x98\xb2\x8f\xbd\x19\x85\xaa\xb6\xbe\xc6\xc2\xea\xd3\x74\xde\x59\x5b\xba\x7d\xe0\xc9\xe4\xbb\xc1\x2e\xa4\xf3\xb8\xd3\x36\x1f\xfe\xbe\x6b\xac\xee\xeb\xff\x2d\x7b\xab\x1f\x36\xd6\xab\x19\x6a\xd3\x71\x89\xd8\x0a\x6f\xb6\x04\xf3\xed\x5d\x6d\x72\xb0\x26\xfb\x0f\x81\xd8\x24\xfd\x4d\x41\x38\x3f\x92\xd5\x9b\x82\xfc\x9c\x22\xb1\xba\x2a\xc8\x39\xdf\x69\xd4\x3a\xdc\x1f\xf8\xf0\x81\x0b\xf3\x7b\x29\xcd\xdc\xfc\x0a\x00\x00\xff\xff\x42\x62\x8b\xfc\x90\x04\x00\x00")

func serviceSwaggerJsonBytes() ([]byte, error) {
	return bindataRead(
		_serviceSwaggerJson,
		"service.swagger.json",
	)
}

func serviceSwaggerJson() (*asset, error) {
	bytes, err := serviceSwaggerJsonBytes()
	if err != nil {
		return nil, err
	}

	info := bindataFileInfo{name: "service.swagger.json", size: 0, mode: os.FileMode(0), modTime: time.Unix(0, 0)}
	a := &asset{bytes: bytes, info: info, digest: [32]uint8{0xa0, 0x12, 0x8b, 0x96, 0xb1, 0x46, 0xc3, 0x37, 0x4b, 0x67, 0x73, 0x55, 0x9, 0xf4, 0xc7, 0xf1, 0xc2, 0xbe, 0xcf, 0x15, 0xea, 0xf, 0xef, 0xd, 0x2, 0x42, 0x93, 0x8e, 0xd9, 0x7f, 0xef, 0x6f}}
	return a, nil
}

// Asset loads and returns the asset for the given name.
// It returns an error if the asset could not be found or
// could not be loaded.
func Asset(name string) ([]byte, error) {
	canonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[canonicalName]; ok {
		a, err := f()
		if err != nil {
			return nil, fmt.Errorf("Asset %s can't read by error: %v", name, err)
		}
		return a.bytes, nil
	}
	return nil, fmt.Errorf("Asset %s not found", name)
}

// AssetString returns the asset contents as a string (instead of a []byte).
func AssetString(name string) (string, error) {
	data, err := Asset(name)
	return string(data), err
}

// MustAsset is like Asset but panics when Asset would return an error.
// It simplifies safe initialization of global variables.
func MustAsset(name string) []byte {
	a, err := Asset(name)
	if err != nil {
		panic("asset: Asset(" + name + "): " + err.Error())
	}

	return a
}

// MustAssetString is like AssetString but panics when Asset would return an
// error. It simplifies safe initialization of global variables.
func MustAssetString(name string) string {
	return string(MustAsset(name))
}

// AssetInfo loads and returns the asset info for the given name.
// It returns an error if the asset could not be found or
// could not be loaded.
func AssetInfo(name string) (os.FileInfo, error) {
	canonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[canonicalName]; ok {
		a, err := f()
		if err != nil {
			return nil, fmt.Errorf("AssetInfo %s can't read by error: %v", name, err)
		}
		return a.info, nil
	}
	return nil, fmt.Errorf("AssetInfo %s not found", name)
}

// AssetDigest returns the digest of the file with the given name. It returns an
// error if the asset could not be found or the digest could not be loaded.
func AssetDigest(name string) ([sha256.Size]byte, error) {
	canonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[canonicalName]; ok {
		a, err := f()
		if err != nil {
			return [sha256.Size]byte{}, fmt.Errorf("AssetDigest %s can't read by error: %v", name, err)
		}
		return a.digest, nil
	}
	return [sha256.Size]byte{}, fmt.Errorf("AssetDigest %s not found", name)
}

// Digests returns a map of all known files and their checksums.
func Digests() (map[string][sha256.Size]byte, error) {
	mp := make(map[string][sha256.Size]byte, len(_bindata))
	for name := range _bindata {
		a, err := _bindata[name]()
		if err != nil {
			return nil, err
		}
		mp[name] = a.digest
	}
	return mp, nil
}

// AssetNames returns the names of the assets.
func AssetNames() []string {
	names := make([]string, 0, len(_bindata))
	for name := range _bindata {
		names = append(names, name)
	}
	return names
}

// _bindata is a table, holding each asset generator, mapped to its name.
var _bindata = map[string]func() (*asset, error){
	"service.swagger.json": serviceSwaggerJson,
}

// AssetDir returns the file names below a certain
// directory embedded in the file by go-bindata.
// For example if you run go-bindata on data/... and data contains the
// following hierarchy:
//     data/
//       foo.txt
//       img/
//         a.png
//         b.png
// then AssetDir("data") would return []string{"foo.txt", "img"},
// AssetDir("data/img") would return []string{"a.png", "b.png"},
// AssetDir("foo.txt") and AssetDir("notexist") would return an error, and
// AssetDir("") will return []string{"data"}.
func AssetDir(name string) ([]string, error) {
	node := _bintree
	if len(name) != 0 {
		canonicalName := strings.Replace(name, "\\", "/", -1)
		pathList := strings.Split(canonicalName, "/")
		for _, p := range pathList {
			node = node.Children[p]
			if node == nil {
				return nil, fmt.Errorf("Asset %s not found", name)
			}
		}
	}
	if node.Func != nil {
		return nil, fmt.Errorf("Asset %s not found", name)
	}
	rv := make([]string, 0, len(node.Children))
	for childName := range node.Children {
		rv = append(rv, childName)
	}
	return rv, nil
}

type bintree struct {
	Func     func() (*asset, error)
	Children map[string]*bintree
}

var _bintree = &bintree{nil, map[string]*bintree{
	"service.swagger.json": &bintree{serviceSwaggerJson, map[string]*bintree{}},
}}

// RestoreAsset restores an asset under the given directory.
func RestoreAsset(dir, name string) error {
	data, err := Asset(name)
	if err != nil {
		return err
	}
	info, err := AssetInfo(name)
	if err != nil {
		return err
	}
	err = os.MkdirAll(_filePath(dir, filepath.Dir(name)), os.FileMode(0755))
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(_filePath(dir, name), data, info.Mode())
	if err != nil {
		return err
	}
	return os.Chtimes(_filePath(dir, name), info.ModTime(), info.ModTime())
}

// RestoreAssets restores an asset under the given directory recursively.
func RestoreAssets(dir, name string) error {
	children, err := AssetDir(name)
	// File
	if err != nil {
		return RestoreAsset(dir, name)
	}
	// Dir
	for _, child := range children {
		err = RestoreAssets(dir, filepath.Join(name, child))
		if err != nil {
			return err
		}
	}
	return nil
}

func _filePath(dir, name string) string {
	canonicalName := strings.Replace(name, "\\", "/", -1)
	return filepath.Join(append([]string{dir}, strings.Split(canonicalName, "/")...)...)
}

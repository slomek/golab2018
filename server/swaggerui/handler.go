package swaggerui

import (
	"bytes"
	"html/template"
	"net/http"

	assetfs "github.com/elazarl/go-bindata-assetfs"
)

func NewMux(swaggerFileURL string) *http.ServeMux {
	mux := http.NewServeMux()

	indexTplString, err := Asset("index.html.tmpl")
	if err != nil {
		panic(err)
	}
	indexTpl := template.Must(template.New("").Parse(string(indexTplString)))

	var buff bytes.Buffer
	indexTpl.Execute(&buff, map[string]interface{}{
		"swaggerfile": swaggerFileURL,
	})

	mux.HandleFunc("/", func(rw http.ResponseWriter, req *http.Request) {
		rw.Header().Set("content-type", "text/html")
		rw.Write(buff.Bytes())
	})

	mux.Handle("/static/",
		http.StripPrefix("/static",
			http.FileServer(
				&assetfs.AssetFS{Asset: Asset, AssetDir: AssetDir, AssetInfo: AssetInfo})))
	return mux
}

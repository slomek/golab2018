package rpc

import (
	"context"
	"math"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	pb "gitlab.com/slomek/golab2018/proto"
)

type Server struct{}

func (s Server) CalculatePower(ctx context.Context, req *pb.CalculatePowerRequest) (*pb.CalculatePowerResponse, error) {
	if err := req.Validate(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "%s", err.Error())
	}

	return &pb.CalculatePowerResponse{
		Result: math.Pow(req.GetBase(), req.GetPower()),
	}, nil
}

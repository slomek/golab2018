package rpc

import (
	"context"
	"testing"

	pb "gitlab.com/slomek/golab2018/proto"
)

func TestServer_CalculatePower(t *testing.T) {
	srv := Server{}
	resp, err := srv.CalculatePower(context.Background(),
		&pb.CalculatePowerRequest{
			Base:  2.0,
			Power: 4.0,
		})
	if err != nil {
		t.Fatalf("Error: %v", err)
	}
	if want, got := 16.0, resp.GetResult(); want != got {
		t.Errorf("Bad result, expeted %v, got: %v", want, got)
	}
}

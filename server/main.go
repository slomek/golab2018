package main

import (
	"context"
	"log"
	"net"
	"net/http"
	"os"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	pb "gitlab.com/slomek/golab2018/proto"
	"gitlab.com/slomek/golab2018/server/rpc"
	"gitlab.com/slomek/golab2018/server/swaggerui"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

func main() {
	addr := os.Getenv("ADDR")
	if addr == "" {
		addr = ":8000"
	}
	httpAddr := os.Getenv("HTTP_ADDR")
	if httpAddr == "" {
		httpAddr = ":8080"
	}

	creds, err := credentials.NewServerTLSFromFile("server/server-cert.pem", "server/server-key.pem")
	if err != nil {
		log.Fatalf("Failed to setup tls: %v", err)
	}

	lis, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatalf("failed to initialize TCP listener: %v", err)
	}
	defer lis.Close()

	server := grpc.NewServer(
		grpc.Creds(creds),
	)
	pb.RegisterPowerServer(server, rpc.Server{})

	go func() {
		swaggerDocs, err := pb.Asset("service.swagger.json")
		if err != nil {
			log.Fatalf("Failed to find Swagger documentation: %v", err)
		}

		creds, err := credentials.NewClientTLSFromFile("server/server-cert.pem", "localhost")
		if err != nil {
			log.Fatalf("gateway cert load error: %s", err)
		}
		opts := []grpc.DialOption{grpc.WithTransportCredentials(creds)}

		mux := http.NewServeMux()
		mux.Handle("/_/swagger/", http.StripPrefix("/_/swagger", swaggerui.NewMux("/_/swagger.json")))
		mux.HandleFunc("/_/swagger.json", func(w http.ResponseWriter, req *http.Request) {
			w.WriteHeader(http.StatusOK)
			w.Header().Set("Content-type", "application/json; charset=utf8")
			w.Write(swaggerDocs)
		})

		gw := runtime.NewServeMux()
		if err := pb.RegisterPowerHandlerFromEndpoint(context.Background(), gw, addr, opts); err != nil {
			log.Fatalf("failed to start HTTP server: %v", err)
		}
		mux.Handle("/", gw)

		log.Printf("HTTP Listening on %s\n", httpAddr)
		log.Fatal(http.ListenAndServe(httpAddr, mux))
	}()

	log.Printf("gRPC Listening on %s\n", lis.Addr().String())
	if err := server.Serve(lis); err != nil {
		log.Fatal(err)
	}
}

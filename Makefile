all:
	echo "nah"

gen_proto:
	protoc -I . --go_out=plugins=grpc:${GOPATH}/src proto/service.1.proto

gen_proto_gw:
	protoc -I . -I ${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis --go_out=plugins=grpc:${GOPATH}/src --grpc-gateway_out=logtostderr=true:${GOPATH}/src proto/service.proto

gen_proto_swagger:
	protoc -I . -I ${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis --go_out=plugins=grpc:${GOPATH}/src --grpc-gateway_out=logtostderr=true:${GOPATH}/src --swagger_out=logtostderr=true:. proto/service.proto
	go generate proto/swagger.go

gen_server_tls:
	openssl req -x509 -newkey rsa:4096 -keyout server/server-key.pem -out server/server-cert.pem -days 365 -nodes -subj '/CN=localhost'



package main

import (
	"context"
	"log"
	"os"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	pb "gitlab.com/slomek/golab2018/proto"
)

func main() {
	serverAddr := os.Getenv("SERVER_ADDR")
	if serverAddr == "" {
		serverAddr = "localhost:8000"
	}

	creds, err := credentials.NewClientTLSFromFile("server/server-cert.pem", "")
	if err != nil {
		log.Fatalf("cert load error: %s", err)
	}

	conn, err := grpc.Dial(serverAddr, grpc.WithTransportCredentials(creds))
	if err != nil {
		log.Fatalf("Failed to start gRPC connection: %v", err)
	}
	defer conn.Close()

	client := pb.NewPowerClient(conn)

	base := 2.0
	power := 3.0

	res, err := client.CalculatePower(context.Background(), &pb.CalculatePowerRequest{
		Base:  base,
		Power: power,
	})
	if err != nil {
		log.Fatalf("Failed to calculate power: %v", err)
	}
	log.Printf("Power result for base=%f and power=%f is %f\n", base, power, res.GetResult())
}
